import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Button } from 'reactstrap';
import Header from './components/header';
import SideBar from './components/sidebar';
import Experiences from './pages/experiences';

function App() {
  return (
    <React.Fragment>
      <Header />
      <div className="d-flex">
        <SideBar />
        <div id="page-content-wrapper">
          <Experiences />
        </div>
      </div>

    </React.Fragment>
  );
}

export default App;
