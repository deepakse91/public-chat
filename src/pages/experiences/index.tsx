import React from 'react';
import Video from './video';
import PublicChat from './chat';

const Experiences = () => {
    return (
        <React.Fragment>
            <div className="row no-gutters">
                <div className="col-lg-8">
                    <Video />
                </div>
                <div className="col-lg-4">
                    <PublicChat />
                </div>
            </div>
        </React.Fragment>
    )
}

export default Experiences

