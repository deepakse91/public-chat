import React, { useState } from 'react'
import { Nav, NavItem, NavLink, TabContent, TabPane, Row, Col, Card, CardTitle, CardText, Button, CardBody } from 'reactstrap';
import classnames from 'classnames';

interface ILiveChat {
  head: string,
  value: string
}

const liveChatArray: ILiveChat[] = [
  {
    head: "Public Chat",
    value: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  },
  {
    head: "Public Chat",
    value: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  },
  {
    head: "Public Chat",
    value: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  },
  {
    head: "Public Chat",
    value: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  },
  {
    head: "Public Chat",
    value: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  },
  {
    head: "Public Chat",
    value: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  }
];

function PublicChat() {
  const [activeTab, setActiveTab] = useState('1');
  const [stateLike, setStateLike] = useState(false);
  const [stateLike1, setStateLike1] = useState(false);
  const [stateMassege, setStateMassege] = useState(false);

  const toggle = (tab: any) => {
    if (activeTab !== tab) setActiveTab(tab);
  }

  const likeHand = () => {
    setStateLike(!stateLike);
  }

  const likeHand1 = () => {
    setStateLike1(!stateLike1);
  }

  const showMsg = () => {
    setStateMassege(true);
  }

  const handleChange = (event: any) => {
    if (event.key === 'Enter') {
      setStateMassege(true);
    }
  }

  return (
    <React.Fragment>
      <div className="bg-chat">
        <div className="position-relative">
          <input type="text" className="form-control pl-5 font-weight-bold" placeholder="Public chat" disabled />
          <img className="chat-icon" src={process.env.PUBLIC_URL + "/assets/images/chatimg.png"} alt="map" />
          <img className="right-icon" src={process.env.PUBLIC_URL + "/assets/images/right-icon.png"} alt="map" />
        </div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '1' })}
              onClick={() => { toggle('1') }}
            >
              Live Chat
          </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '2' })}
              onClick={() => { toggle('2') }}
            >
              Q&A
          </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <Card className="bg-transparent">
              <CardBody className="chatoverflow">
                {
                  liveChatArray.length > 0 && liveChatArray.map((item: ILiveChat, key: number) => {
                    return (
                      <div className={`d-flex ${key === 0 ? '' : 'mt-2'}`}>
                        <div>
                          <img className="circle-img" src={process.env.PUBLIC_URL + "/assets/images/Ellipse 4.png"} alt="map" />
                        </div>
                        <div className="ml-4">
                          <h5 className="text-white font18">{item.head}</h5>
                          <p className="text-white font12">{item.value}</p>
                        </div>
                      </div>
                    )
                  })
                }
              </CardBody>
            </Card>
          </TabPane>

          <TabPane tabId="2">
            <Card className="bg-transparent">
              <CardBody className="chatoverflow">
                <div className="list-group">
                  <a className={`list-group-item list-group-item-action ${stateLike ? 'active' : ''}`}>
                    <div className="d-flex align-items-center">
                      <div>
                        <h5 className="text-white font18">How to do a virtual event?</h5>
                        <p className="text-white font12 mb-0">The way we conduct a physical event , Virtual events can also be conducted in the same way.</p>
                        <p className="text-white-50 font12 mb-0"><em>By ADMIN</em></p>
                      </div>
                      <div className="ml-4">
                        <div className="d-flex" onClick={() => { likeHand() }}>
                          <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.71436 0C8.44659 0.000225 8.19932 0.143145 8.06543 0.375L4.60107 6.375C4.53507 6.48896 4.50018 6.61829 4.5 6.75V17.25C4.50004 17.6642 4.83575 18 5.25 18H15.2417C15.867 18 16.4262 17.7007 16.7827 17.2998C17.1393 16.8989 17.3377 16.4211 17.4653 15.9448L19.4751 8.44482C19.6632 7.74288 19.3477 7.05791 18.9023 6.64746C18.457 6.23702 17.8768 6 17.2515 6H10.9644V2.25C10.9644 1.01647 9.94802 0 8.71436 0ZM0.75 6.75C0.335782 6.75004 4.5e-05 7.0858 0 7.5V16.5C4.5e-05 16.9142 0.335782 17.25 0.75 17.25H3V6.75H0.75Z" />
                          </svg> <span className="text-white ml-3">234</span>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a className={`list-group-item list-group-item-action ${stateLike1 ? 'active' : ''}`}>
                    <div className="d-flex align-items-center">
                      <div>
                        <h5 className="text-white font18">How to do a virtual event?</h5>
                        <p className="text-white mb-0 font12">The way we conduct a physical event , Virtual events can also be conducted in the same way.</p>
                        <p className="text-white-50 font12 mb-0"><em>By ADMIN</em></p>
                      </div>
                      <div className="ml-4">
                        <div className="d-flex" onClick={() => { likeHand1() }}>
                          <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.71436 0C8.44659 0.000225 8.19932 0.143145 8.06543 0.375L4.60107 6.375C4.53507 6.48896 4.50018 6.61829 4.5 6.75V17.25C4.50004 17.6642 4.83575 18 5.25 18H15.2417C15.867 18 16.4262 17.7007 16.7827 17.2998C17.1393 16.8989 17.3377 16.4211 17.4653 15.9448L19.4751 8.44482C19.6632 7.74288 19.3477 7.05791 18.9023 6.64746C18.457 6.23702 17.8768 6 17.2515 6H10.9644V2.25C10.9644 1.01647 9.94802 0 8.71436 0ZM0.75 6.75C0.335782 6.75004 4.5e-05 7.0858 0 7.5V16.5C4.5e-05 16.9142 0.335782 17.25 0.75 17.25H3V6.75H0.75Z" />
                          </svg> <span className="text-white ml-3">234</span>
                        </div>

                      </div>
                    </div>
                  </a>
                </div>
              </CardBody>
            </Card>
          </TabPane>
        </TabContent>

        <div className="mt-4">
          <div className="approval font12" style={{ display: stateMassege ? 'block' : 'none' }}>
            Your question has been sent for approval. It will be Live soon.
          </div>
          <div className="position-relative">
            <input type="text" className="form-control py-5" placeholder="Ask your Question" onKeyPress={handleChange} />

            <img onClick={() => { showMsg() }} className="right-icon right-icon-top" src={process.env.PUBLIC_URL + "/assets/images/Vector.png"} alt="map" />
          </div>
        </div>
      </div>
    </React.Fragment >
  )
}

export default PublicChat

