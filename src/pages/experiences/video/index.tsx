import React from 'react';

const Video = () => {
    return (
        <React.Fragment>
            <div className="bg-video">
                {/* <video controls className="video">
                    <source src="https://www.youtube.com/embed/N3AkSS5hXMA" type="video/mp4" />
                    <source src="https://www.youtube.com/embed/N3AkSS5hXMA" type="video/ogg" />
                </video> */}
                <iframe allowFullScreen={true} width="100%" height="380" src="https://www.youtube.com/embed/JPT3bFIwJYA" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" title="Introduction of React" />

                <div className="card bg-transparent border-0">
                    <div className="card-body">
                        <div className="row align-items-center">
                            <div className="col-md-8">
                                <h5 className="text-white font18">Samuel Johan's Live demo of Recording in Studio</h5>
                                <p className="text-white font12">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                            <div className="col-md-4 text-center text-white">
                                <a className="btn btn-outline-light px-5 py-3 font18 font-weight-bold ">Interact </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Video

