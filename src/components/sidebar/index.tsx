import React, { useState } from 'react';

const SideBar = () => {
   const [isBoxVisible, SetIsBoxVisible] = useState(false);
   const toggleBox = () => {
      SetIsBoxVisible(!isBoxVisible)
   }

   return (
      <React.Fragment>
         <img onClick={toggleBox} className="menu-icon" src={process.env.PUBLIC_URL + "/assets/images/icons8-menu-48.png"} alt="map" />
         <div className={`bg-dark ${isBoxVisible ? "open" : ""}`} id="sidebar-wrapper">
            <div className="list-group list-group-flush" style={{ textAlign: 'center' }}>
               {/* <a className="list-group-item list-group-item-action">Sessions</a>
               <a className="list-group-item list-group-item-action">Networking</a> */}
               <a className="list-group-item list-group-item-action active">Experiences</a>
            </div>
         </div>
      </React.Fragment>
   )
}

export default SideBar

