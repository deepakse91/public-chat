import React from 'react';

const Header = () => {
    return (
        <header>
            <div className="container-fluid">
                <img className="logo" src={process.env.PUBLIC_URL + "/assets/images/Group 7.png"} alt="map" />
            </div>
        </header>
    )
}

export default Header


